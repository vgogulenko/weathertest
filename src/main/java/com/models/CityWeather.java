package com.models;

import java.util.List;
import java.util.Objects;

public class CityWeather implements Comparable<CityWeather> {
    private String base;
    private double visibility;
    private long id;
    private String name;
    private long cod;

    private Coord coord;
    private List<Weather> weather;
    private Main main;
    private Wind wind;
    private Sys sys;
    private Clouds clouds;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public double getVisibility() {
        return visibility;
    }

    public void setVisibility(double visibility) {
        this.visibility = visibility;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCod() {
        return cod;
    }

    public void setCod(long cod) {
        this.cod = cod;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CityWeather that = (CityWeather) o;
        return Double.compare(that.visibility, visibility) == 0 &&
                id == that.id &&
                cod == that.cod &&
                Objects.equals(base, that.base) &&
                Objects.equals(name, that.name) &&
                Objects.equals(coord, that.coord) &&
                Objects.equals(weather, that.weather) &&
                Objects.equals(main, that.main) &&
                Objects.equals(wind, that.wind) &&
                Objects.equals(sys, that.sys) &&
                Objects.equals(clouds, that.clouds);
    }

    @Override
    public int hashCode() {
        return Objects.hash(base, visibility, id, name, cod, coord, weather, main, wind, sys, clouds);
    }

    @Override
    public int compareTo(CityWeather o) {
        return (int) ((this.sys.getSunset() -
                this.sys.getSunrise()) -
                (o.sys.getSunset() - o.sys.getSunrise() ));
    }

    @Override
    public String toString() {
        return "CityWeather{" +
                "base='" + base + '\'' +
                ", visibility=" + visibility +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", cod=" + cod +
                ", coord=" + coord +
                ", weather=" + weather +
                ", main=" + main +
                ", wind=" + wind +
                ", sys=" + sys +
                ", clouds=" + clouds +
                '}';
    }
}
