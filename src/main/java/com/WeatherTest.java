package com;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.models.CityWeather;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;


public class WeatherTest {

    private final static String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    private final static String CELSIUS = "&units=metric";
    private final static String api_key = "2129b0d4a034345575dc2dca4e05a2e6";

    public static void main(String[] args) {
        List<String> citiesList = Arrays.asList("Tel Aviv", "Singapore", "Auckland", "Ushuaia",
                "Miami", "London", "Berlin", "Reykjavik", "Cape Town", "Kathmandu");

        //Run through the list of cities, get weather info for each of them and collect to the map
        Map<String, String> jsonData = citiesList.stream()
                .collect(Collectors.toMap(Function.identity(), s -> getCurrentWeather(s)));
        //create new TreeSet which will be used for sorting by longest and shortest daylight time
        TreeSet<CityWeather> cityWeathers = new TreeSet<>();
        //create ObjectMapper instance
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        //convert json string to object
        jsonData.keySet().forEach(city -> {
            try {
                CityWeather readValue = objectMapper.readValue(jsonData.get(city), CityWeather.class);
                cityWeathers.add(readValue);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        System.out.println("The shortest daylight time today in " + cityWeathers.first().getName()+" " +
                "and temperature there is "+Math.round(cityWeathers.first().getMain().getTemp())+"\u2103");
        System.out.println("The longest daylight time today in " + cityWeathers.last().getName()+" " +
                "and temperature there is "+Math.round(cityWeathers.last().getMain().getTemp())+"\u2103");
    }

    public static String getCurrentWeather(String city) {
        String fullURL = BASE_URL + "weather?q=" + city + CELSIUS + "&appid=" + api_key;
        String line;
        StringBuilder weatherJsonStr = new StringBuilder();
        try {
            URL url = new URL(fullURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) {
                weatherJsonStr.append(line);
            }
            rd.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return weatherJsonStr.toString();
    }
}

